﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    //Le Variables
    public Transform player;
    //public GameEnding gameEnding;

    bool m_IsPlayerInRange;

    float aggroCooldown;

    public WaypointPatrol patrolScript;

    public AudioSource scream;

    //Checks for Hit
    private void OnTriggerEnter(Collider other)
    {
        
        if(other.transform == player)
        {
            m_IsPlayerInRange = true;
            scream.Play();
        }
    }

    //Checks for Leaving
    private void OnTriggerExit(Collider other)
    {
        if(other.transform == player)
        {
            m_IsPlayerInRange = false;
            patrolScript.SetAggro(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

        //Check for Player Inside the Range
        if (m_IsPlayerInRange)
        {
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            if(Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == player)
                {
                    //gameEnding.CaughtPlayer();
                    patrolScript.SetAggro(true);
                    print("True");
                }
                else
                    print("Noticing Something Else");
            }
        }
    }
}
