﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //These are Some Spooky Variables
    public float turnSpeed = 20f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    public float sprintEnergy = 1f;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) && sprintEnergy > 0f)
        {
            m_Animator.speed = 3f;
            sprintEnergy -= Time.deltaTime;
        }
        else
        {
            m_Animator.speed = 1f;
            sprintEnergy += Time.deltaTime * .25f;
            if (sprintEnergy > 1f)
            {
                sprintEnergy = 1f;
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Moves the Player
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();



        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        //Plays Sound if Walking
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    private void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + transform.TransformDirection(m_Movement) * m_Animator.deltaPosition.magnitude); //Moves John Lemon Locally instead of Globally
        //m_Rigidbody.MoveRotation(m_Rotation);
    }
}
