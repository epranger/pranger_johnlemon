﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    //Varialbes Again
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;
    public Animator m_Animator;

    Transform player;
    bool aggro;

    public GameEnding gameEnding;

    int m_CurrentWaypointIndex;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        navMeshAgent.SetDestination(waypoints[0].position);
        m_Animator.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if(!aggro && navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }
    }

    public void SetAggro(bool set)
    {
        aggro = set;
        if (aggro)
        {
            navMeshAgent.SetDestination(player.position);
            navMeshAgent.speed = 2.5f;
            m_Animator.SetBool("LineOfSight", true);
        }

        else
        {
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
            navMeshAgent.speed = 1f;
            m_Animator.SetBool("LineOfSight", false);
        }
            
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform == player)
        {
            gameEnding.CaughtPlayer();
            m_Animator.SetBool("Attack", true);
        }
    }

}
